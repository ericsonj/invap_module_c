#ifdef TEST

#include "unity.h"
#include <string.h>

#include "temp_conv.h"

void setUp(void)
{
}

void tearDown(void)
{
}

void test_temp_conv(void)
{
    /* Test 25.0C -> 77.0F */
    TEST_ASSERT_EQUAL_FLOAT(77.0f, temp_conv(CELSIUS_U, FAHRENHEIT_U, 25.0f));
    
    /* Test 25.0C -> 298.15F */
    TEST_ASSERT_EQUAL_FLOAT(298.15f, temp_conv(CELSIUS_U, KELVIN_U, 25.0f));

    /* Test 32F -> 0C */
    TEST_ASSERT_EQUAL_FLOAT(0.0f, temp_conv(FAHRENHEIT_U, CELSIUS_U, 32.0f));  
    
    /* Test 32F -> 273.15K */
    TEST_ASSERT_EQUAL_FLOAT(0.0f, temp_conv(FAHRENHEIT_U, KELVIN_U, 32.0f));  
    
    /* Test 301.15F -> 28C */
    TEST_ASSERT_EQUAL_FLOAT(28.0f, temp_conv(KELVIN_U, CELSIUS_U, 301.15f));

    /* Test 301.15F -> 82.4F */
    TEST_ASSERT_EQUAL_FLOAT(82.4f, temp_conv(KELVIN_U, FAHRENHEIT_U, 301.15f));
  
}

void test_temp_conv_snprint(void)
{
    char buffer[32];
    memset(buffer, 0, sizeof(buffer));
    temp_conv_snprint(buffer, sizeof(buffer), CELSIUS_U, 25.0f, FAHRENHEIT_U, 77.0f, KELVIN_U, 298.15, NULL);
    TEST_ASSERT_EQUAL_STRING("25.00C 77.00F 298.15K", buffer);
}


#endif // TEST
